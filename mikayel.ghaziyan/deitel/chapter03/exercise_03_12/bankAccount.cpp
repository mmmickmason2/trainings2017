#include "bankAccount.hpp"
#include <iostream>
#include <string>


Account::Account(int initialBalance)
{
    setBankAccount(initialBalance);
}

void 
Account::setBankAccount(int initialBalance)
{
    if (initialBalance < 0) {

	accountBalance_ = 0;

	std::cout << "Info 1. The initial balance cannot be a negative number" << std::endl;

	return;
    }	

    accountBalance_ = initialBalance;
}

void
Account::credit(int deposit)
{
    accountBalance_ += deposit;
}

void
Account::debit(int withdraw)
{
    if (withdraw > accountBalance_){

	std::cout << "Info 2. The amount that you are willing to withdraw exceeds your account balance" << std::endl;

	return;
    }

    accountBalance_ -= withdraw;
}

int 
Account::getBalance()
{
    return accountBalance_;
}

