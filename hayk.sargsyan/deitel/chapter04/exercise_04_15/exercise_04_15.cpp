#include <iostream>

int
main()
{
    while (true) {
        float moneyFromSales;
        
        std::cout << "Please enter the value of sales in dollars (-1 to exit): ";
        std::cin >> moneyFromSales;

        if (-1 == moneyFromSales) {
            return 0;
        }
        if (moneyFromSales < 0) {
            std::cerr << "Error 1: Value of sales can't be negative" << std::endl;
            return 1;
        }

        std::cout << "Income: " << 200 + moneyFromSales / 100 * 9 << std::endl;
    }

    return 0;
}
