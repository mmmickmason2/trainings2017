#include <iostream>

int
main()
{
    int number;
    std::cout << "Enter binary number: ";
    std::cin >> number;

    if (number < 0) {
        std::cerr << "Error 1. Entered number should be non-negative." << std::endl;
        return 1;
    }

    int counter = 0, decimal = 0, tenPower = 1, twoPower = 1;
    while ((number / tenPower) != 0) {
        int current = number / tenPower % 10;
        if (current != 0) {
            if (current != 1) {
                std::cerr << "Error 2. Entered number isn't binary." << std::endl;
                return 2;
            }
        }
        decimal += current * twoPower;
        tenPower *= 10;
        twoPower *= 2;
        ++counter;
    }
    std::cout << number << "'s decimal equivalent is " << decimal << std::endl;
    return 0;
}
