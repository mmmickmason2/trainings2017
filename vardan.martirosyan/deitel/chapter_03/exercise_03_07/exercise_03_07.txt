Usually a class consists of one or more element-functions that
manipulate attributes belonging to a specific object of the given
class. Attributes are represented by variables in the class definition. These
variables are called data items and are declared inside the definition
class, but outside the body of the definitions of its element-functions.
Each class object stores its own instance of its attributes in memory.
