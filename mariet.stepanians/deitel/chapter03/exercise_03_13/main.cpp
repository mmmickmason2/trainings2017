#include "Invoice.hpp" 
#include <iostream> 
#include <string>

int
main()
{
    std::string numberOfProduct;
    std::string descriptionOfProduct;
    int quantityOfProduct = 0;
    int priceOfProduct = 0;
    
    Invoice invoice(numberOfProduct, descriptionOfProduct, quantityOfProduct, priceOfProduct);
    
    std::cout << "Number: ";
    std::getline(std::cin, numberOfProduct);
    invoice.setNumber(numberOfProduct);
    
    std::cout << "Description: ";
    std::getline(std::cin, descriptionOfProduct);
    invoice.setDescription(descriptionOfProduct);
              
    std::cout << "Quantity: ";
    std::cin  >> quantityOfProduct;
    invoice.setQuantity(quantityOfProduct);
        
    std::cout << "Price: ";
    std::cin  >> priceOfProduct;
    invoice.setPricePerItem(priceOfProduct); 
    
    std::cout << "!!! Overall Amount: " << invoice.getInvoiceAmount() << std::endl; 
    
    return 0; 
} 
