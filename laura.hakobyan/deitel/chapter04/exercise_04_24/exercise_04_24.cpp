#include <iostream>

int 
main ()
{

    int x, y;
    std::cout << "Enter x: ";
    std::cin >> x;
    std::cout << "Enter y: ";
    std::cin >> y;
   
    /*
    /// a. Assum ing x = 5 a nd y = 8, the following output is produced.
    ///@@@@@ 
    ///$$$$$
    ///&&&&&
    if (8 == y)
        if (5 == x)
            std::cout << "@@@@@" << std::endl;
        else
            std::cout << "#####" << std::endl;
    std::cout << "$$$$$" << std::endl;
    std::cout << "&&&&&" << std::endl;
    */

    /*
    /// b. Assum ing x = 5 a nd y = 8, the following output is produced.
    ///@@@@@ 
    if (8 == y) {
        if (5 == x) 
        std::cout << "@@@@@" << std::endl;
    }
    else {
        std::cout << "#####" << std::endl;
        std::cout << "$$$$$" << std::endl;
        std::cout << "&&&&&" << std::endl;
    } 
    */
    
    /* 
    /// c. Assum ing x = 5 a nd y = 8, the following output is produced.
    ///@@@@@ 
    ///&&&&&
    if (8 == y) {
        if (5 == x) {
        std::cout << "@@@@@" << std::endl;
        } else { 
            std::cout << "#####" << std::endl;
            std::cout << "$$$$$" << std::endl;
        }
        std::cout << "&&&&&" << std::endl;
    }
    */

    /// d. Assum ing x = 5 a nd y = 7, the following output is produced.
    ///#####
    ///$$$$$
    ///&&&&&
    if (8 == y) {
        if (5 == x) {
            std::cout << "@@@@@" << std::endl;
        } 
    } else {
        std::cout << "#####" << std::endl;
        std::cout << "$$$$$" << std::endl;
        std::cout << "&&&&&" << std::endl;
    }
    return 0;
}

