#include <iostream>

int
main()
{
    double side1;
    std::cout << "Enter value of side1: ";
    std::cin >> side1;
    if (side1 <= 0) {
        std::cout << "Error 1: Side's value can not be negative." << std::endl;
        return 1;
    }

    double side2;
    std::cout << "Enter value of side2: ";
    std::cin >> side2;
    if (side2 <= 0) {
        std::cout << "Error 1: Side's value can not be negative." << std::endl;
        return 1;
    }

    double side3;
    std::cout << "Enter value of side3: ";
    std::cin >> side3;
    if (side3 <= 0) {
        std::cout << "Error 1: Side's value can not be negative." << std::endl;
        return 1;
    }
    if (side1 + side2 > side3) {
        if (side2 + side3 > side1) {
            if (side1 + side3 > side2) {
                std::cout << side1 << ", " << side2 << " and " << side3 << " are sides of a triangle." << std::endl;
                return 0;
            }
        }
    }
    std::cout << "Triangle cannot be with these parameters." << std::endl;
    return 0;
}

