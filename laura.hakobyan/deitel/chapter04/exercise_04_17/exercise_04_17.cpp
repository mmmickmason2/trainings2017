#include <iostream>

int
main()
{
    int number = -2147483648, counter = 1;
    int largest = number;
    while (counter <= 10) {
        std::cout << "Enter current numbers: ";
        std::cin >> number;
        std::cout << counter << std::endl;

        if (number > largest) {
            largest = number;
        }
        counter++;
    }
    std::cout << "Largest number is " << largest << std::endl;
    return 0;
}
