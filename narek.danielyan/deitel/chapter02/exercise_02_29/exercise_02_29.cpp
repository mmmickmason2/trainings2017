#include <iostream>

int 
main()
{ 
    std::cout << "number\t" << "square\t" << "cube" << std::endl;
    std::cout << "0\t" << 0 * 0 << "\t" << 0 * 0 * 0 << "\t" << std::endl;
    std::cout << "1\t" << 1 * 1 << "\t" << 1 * 1 * 1 << "\t" << std::endl;
    std::cout << "2\t" << 2 * 2 << "\t" << 2 * 2 * 2 << "\t" << std::endl;
    std::cout << "3\t" << 3 * 3 << "\t" << 3 * 3 * 3 << "\t" << std::endl;
    std::cout << "4\t" << 4 * 4 << "\t" << 4 * 4 * 4 << "\t" << std::endl;
    std::cout << "5\t" << 5 * 5 << "\t" << 5 * 5 * 5 << "\t" << std::endl;
    std::cout << "6\t" << 6 * 6 << "\t" << 6 * 6 * 6 << "\t" << std::endl;
    std::cout << "7\t" << 7 * 7 << "\t" << 7 * 7 * 7 << "\t" << std::endl;
    std::cout << "8\t" << 8 * 8 << "\t" << 8 * 8 * 8 << "\t" << std::endl;
    std::cout << "9\t" << 9 * 9 << "\t" << 9 * 9 * 9 << "\t" << std::endl;
    std::cout << "10\t" << 10 * 10 << "\t" << 10 * 10 * 10 << "\t" << std::endl;
    return 0;
}
